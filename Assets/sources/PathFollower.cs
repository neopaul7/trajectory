﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFollower : MonoBehaviour
{
    [SerializeField]
    PathDraw pathDraw;
    [SerializeField]
    GameObject fallower;

    IPointDataShare pointsForFollower;

    float timer, speed = 2;
    int curPointNum, play = 1, maxPosition;
    Vector3 dir, curGoalPosition = new Vector3();
    bool isMobilePlatform, go = false;

    void Start()
    {
        pointsForFollower = (IPointDataShare)pathDraw;
        pointsForFollower.IncPointEvent += maxPositionHandler;
    }

    void maxPositionHandler(int maxPosition)
    {
        this.maxPosition = maxPosition;
        go = true;
    }

    void Update()
    {
        if (play > 0 && go)
            if (timer > .03)
            {
                if (maxPosition >= curPointNum)
                {
                    fallower.transform.position += (transform.up * dir.y + transform.right * dir.x) * speed * Time.deltaTime;

                    if (Vector3.Distance(fallower.transform.position, curGoalPosition) < .3)
                    {
                        pointsForFollower.DestroyPointByIndex(curPointNum);

                        if (maxPosition > curPointNum)
                        {
                            curGoalPosition = pointsForFollower.GetPointPositionByIndex(++curPointNum);
                            dir = (curGoalPosition - fallower.transform.position).normalized;
                            fallower.transform.up = dir;
                        }
                        else
                            go = false;
                    }
                }
            }
            else timer += Time.deltaTime;
    }

    public void PlayStop()
    {
        play *= -1;
    }
}
