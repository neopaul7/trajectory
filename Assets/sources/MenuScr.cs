﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuScr : MonoBehaviour
{
    public void CloseAppHandler()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.Android:
                AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
                activity.Call<bool>("moveTaskToBack", true);
                break;
            case RuntimePlatform.WindowsPlayer:
                Application.Quit();
                break;/*
            default:
                UnityEditor.EditorApplication.isPlaying = false;
                break;*/
        }
    }
}
