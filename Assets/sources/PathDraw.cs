﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public delegate void IncPointDeleg(int pointCounters);
public class PathDraw : MonoBehaviour, IPointDataShare
{
    List<Vector3> pathPointList = new List<Vector3>();

    LineRenderer lineRenderer;
    [SerializeField]
    private GameObject goalGObj;
    Vector3 dir;
    List<GameObject> goalGameObjects = new List<GameObject>();
    bool isMobilePlatform;
    public event IncPointDeleg IncPointEvent;

    // init and cfg
    void Start()
    {
        goalGameObjects.Add(new GameObject());
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.SetWidth(.125f,.125f);
        isMobilePlatform = Application.platform == RuntimePlatform.Android;
    }

    bool isClickOnGUIFreeScreen()
    {
        if (isMobilePlatform)
        {
            if (EventSystem.current.IsPointerOverGameObject(0))
                return false;
        }
        else
        {
            if (EventSystem.current.IsPointerOverGameObject())
                return false;
        }

        return true;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && isClickOnGUIFreeScreen())
        {
            lineRenderer.positionCount++;
            lineRenderer.SetPosition(lineRenderer.positionCount - 1, Camera.main.ScreenToWorldPoint(Input.mousePosition) + transform.forward * 10);
            goalGameObjects.Add(Instantiate(goalGObj, lineRenderer.GetPosition(lineRenderer.positionCount - 1), Quaternion.identity));
            IncPointEvent(lineRenderer.positionCount-1); // send max avalible number of position
        }
    }

    //interface implementations
    public Vector3 GetPointPositionByIndex(int curPointNum)
    {
        return lineRenderer.GetPosition(curPointNum);
    }

    public void DestroyPointByIndex(int curPointNum)
    {
        Destroy(goalGameObjects[curPointNum]);
    }
}

public interface IPointDataShare
{
    Vector3 GetPointPositionByIndex(int curPointNum);

    void DestroyPointByIndex(int curPoint);

    event IncPointDeleg IncPointEvent;
}
